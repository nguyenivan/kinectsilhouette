﻿using UnityEngine;
using System.Collections;
using OpenCvSharp;
using System.Runtime.InteropServices;

public class FirstCV : MonoBehaviour
{

    void Start()
    {
        using (IplImage src = Cv.LoadImage("Snapshots/screen_1920x1080_2014-12-24_10-23-14.png"))
        {
            using (IplImage gray = new IplImage(src.Size, BitDepth.U8, 1))
            {
                using (IplImage bin = new IplImage(src.Size, BitDepth.U8, 1))
                {
                    src.CvtColor(gray, ColorConversion.RgbaToGray);
                    Cv.Smooth(gray,gray, SmoothType.Gaussian, 9, 9, 4.0);
                    CvMemStorage storage = Cv.CreateMemStorage(0);
                    CvSeq<CvPoint> contour = null;
                    gray.Threshold(bin, 192.0, 255.0, ThresholdType.BinaryInv);
                    Cv.FindContours(bin, storage, out contour, OpenCvSharp.CvContour.SizeOf, ContourRetrieval.External, ContourChain.ApproxNone);
                    Cv.DrawContours(src, contour, new CvScalar(100), new CvScalar(100), 10, -1);
                    //using (IplImage tmpImg = src.Clone())
                    //{
                    //    IplConvKernel element = Cv.CreateStructuringElementEx(50, 50, 0,0, OpenCvSharp.ElementShape.Ellipse);
                    //    Cv.MorphologyEx(src, src,tmpImg, element, MorphologyOperation.Gradient);
                    //}
                    Cv.ShowImage("Photo", src);
                }
            }
        }
    }

}