﻿using UnityEngine;
using System.Collections;

public class Silhouette : MonoBehaviour {

    public Vector2 screenPoint;
    Plane plane;


	// Use this for initialization
	void Start () {
        this.plane = new Plane(-Vector3.forward, 10);
	}
	
	// Update is called once per frame
	void Update () {
        //TODO need to transform this to the screen plane
        var worldPoint = new Vector3(screenPoint.x, screenPoint.y, .0f);
	    // Cast a ray through screen point
        //var ray = Camera.main.ViewportPointToRay(worldPoint);
        var camera = Camera.main.transform;
        var ray = new Ray(camera.position, worldPoint - camera.position);
        float rayDistance;
        if (this.plane.Raycast(ray, out rayDistance))
        {
            transform.position = ray.GetPoint(rayDistance);
        }
        
	}
}
