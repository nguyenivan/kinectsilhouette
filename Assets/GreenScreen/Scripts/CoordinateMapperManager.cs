using UnityEngine;
using System.Collections;
using Windows.Kinect;
using System.Runtime.InteropServices;
using System;
using OpenCvSharp;

public class CoordinateMapperManager : MonoBehaviour
{

    public Texture2D onButton;
    public Texture2D offButton;

    private KinectSensor m_pKinectSensor;
    private CoordinateMapper m_pCoordinateMapper;
    private MultiSourceFrameReader m_pMultiSourceFrameReader;
    private DepthSpacePoint[] m_pDepthCoordinates;

    private byte[] pColorBuffer;
    private byte[] pBodyIndexBuffer;

    private ushort[] pDepthBuffer;

    private byte[] pSilhouetteBuffer;

    const int cDepthWidth = 512;
    const int cDepthHeight = 424;
    const int cColorWidth = 1920;
    const int cColorHeight = 1080;

    Rect bigRect;
    Rect smallRect;

    long frameCount = 0;

    double elapsedCounter = 0.0;
    double fps = 0.0;

    Texture2D m_pColorRGBX;
    Texture2D m_pSilhouetteX;

    bool nullFrame = false;

    void Awake()
    {
        pColorBuffer = new byte[cColorWidth * cColorHeight * 4];
        pBodyIndexBuffer = new byte[cDepthWidth * cDepthHeight];
        pDepthBuffer = new ushort[cDepthWidth * cDepthHeight];
        pSilhouetteBuffer = new byte[cColorWidth * cColorHeight];

        m_pSilhouetteX = new Texture2D(cColorWidth, cColorHeight, TextureFormat.RGBA32, false);
        m_pDepthCoordinates = new DepthSpacePoint[cColorWidth * cColorHeight];

        InitializeDefaultSensor();

        this.plane = new Plane(-Vector3.forward, 10);

        this.bigRect = new Rect(
            (Screen.width - onButton.width) / 2,
            (Screen.height - onButton.height) / 2,
            onButton.width,
            onButton.height
        );

        this.smallRect = new Rect(
            15,
            15,
            offButton.width / 3,
            offButton.height  / 3
        );
        this.currentRect = bigRect;
    }

    Rect fpsRect = new Rect(10, 10, 200, 30);
    Rect nullFrameRect = new Rect(10, 50, 200, 30);
    Body[] bodies = null;
    Plane plane;
    private bool inGame = false;
    private Rect currentRect;

    void OnGUI()
    {
        GUI.Box(fpsRect, "FPS: " + fps.ToString("0.00"));

        if (nullFrame)
        {
            GUI.Box(nullFrameRect, "NULL MSFR Frame");
        }

        if (!inGame) {
            if (GUI.Button(currentRect, onButton))
            {
                StartCoroutine(ZoomButtonOut(1f));
            }
        }
        else
        {
            if (GUI.Button(currentRect, offButton))
            {
                StartCoroutine(ZoomButtonIn(1f));
            }
        }

    }

    IEnumerator ZoomButtonOut(float time)
    {
        var elapsedTime = 0f;
        while (elapsedTime < time)
        {
            var newCenter = Vector2.Lerp(bigRect.center, smallRect.center, (elapsedTime / time));
            var newSize = Vector2.Lerp(bigRect.size, smallRect.size, (elapsedTime / time));
            this.currentRect.center = newCenter;
            this.currentRect.size = newSize;
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        this.inGame = true;
    }

    IEnumerator ZoomButtonIn(float time)        
    {
        var elapsedTime = 0f;
        while (elapsedTime < time)
        {
            var newCenter = Vector2.Lerp(smallRect.center, bigRect.center, (elapsedTime / time));
            var newSize = Vector2.Lerp(smallRect.size, bigRect.size, (elapsedTime / time));
            this.currentRect.center = newCenter;
            this.currentRect.size = newSize;
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        this.inGame = false;
    }

    public Texture2D GetColorTexture()
    {
        return m_pSilhouetteX;
    }

    public byte[] GetBodyIndexBuffer()
    {
        return pBodyIndexBuffer;
    }

    public SkeletonEnd GetSkeletonEnd(JointType joint)
    {
        if (this.bodies == null)
        {
            return null;
        }
        Body body = null;
        foreach (var b in this.bodies)
        {
            if (b.IsTracked) // track first body 
            {
                body = b;
                break;
            }
        }
        if (body == null) return null;
        var camera = Camera.main;
        var theJoint = body.Joints[joint];
        var theOrientation = body.JointOrientations[joint].Orientation;
        var colorPoint = this.m_pCoordinateMapper.MapCameraPointToColorSpace(theJoint.Position);
        var screenPoint = new Vector3(colorPoint.X, colorPoint.Y, 0f);
        var ret = new SkeletonEnd(
            screenPoint,
            new UnityEngine.Quaternion(
                theOrientation.X,
                theOrientation.Y,
                theOrientation.Z,
                theOrientation.W
            )
        );
        return ret;
    }

    public DepthSpacePoint[] GetDepthCoordinates()
    {
        return m_pDepthCoordinates;
    }

    void InitializeDefaultSensor()
    {
        m_pKinectSensor = KinectSensor.GetDefault();

        if (m_pKinectSensor != null)
        {
            // Initialize the Kinect and get coordinate mapper and the frame reader
            m_pCoordinateMapper = m_pKinectSensor.CoordinateMapper;

            m_pKinectSensor.Open();
            if (m_pKinectSensor.IsOpen)
            {
                m_pMultiSourceFrameReader = m_pKinectSensor.OpenMultiSourceFrameReader(
                    FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.BodyIndex |
                    FrameSourceTypes.Body);
            }
        }

        if (m_pKinectSensor == null)
        {
            UnityEngine.Debug.LogError("No ready Kinect found!");
        }
    }

    void ProcessFrame()
    {
        var pDepthData = GCHandle.Alloc(pDepthBuffer, GCHandleType.Pinned);
        var pDepthCoordinatesData = GCHandle.Alloc(m_pDepthCoordinates, GCHandleType.Pinned);

        m_pCoordinateMapper.MapColorFrameToDepthSpaceUsingIntPtr(
            pDepthData.AddrOfPinnedObject(),
            (uint)pDepthBuffer.Length * sizeof(ushort),
            pDepthCoordinatesData.AddrOfPinnedObject(),
            (uint)m_pDepthCoordinates.Length);

        pDepthCoordinatesData.Free();
        pDepthData.Free();

        for (int colorIndex = 0; colorIndex < cColorWidth * cColorHeight; colorIndex++)
        {
            var depthCoord = m_pDepthCoordinates[colorIndex];
            if ((!float.IsInfinity(depthCoord.X) && !float.IsNaN(depthCoord.X) && depthCoord.X != 0) &&
                (!float.IsInfinity(depthCoord.Y) && !float.IsNaN(depthCoord.Y) && depthCoord.Y != 0))
            {
                float player = pBodyIndexBuffer[(int)depthCoord.X + (int)depthCoord.Y * cDepthWidth];
                if (player != 255)
                {
                    pSilhouetteBuffer[colorIndex] = 0; // black if has bodyIndex
                    continue;
                }
            }
            pSilhouetteBuffer[colorIndex] = 255; // white 
        }


        // Load the image from memory
        using (CvMat matBuf = new CvMat(cColorHeight, cColorWidth, MatrixType.U8C1, pSilhouetteBuffer))
        {
            using (IplImage bin = new IplImage(matBuf.GetSize(), BitDepth.U8, 1))
            {
                using (CvMemStorage storage = Cv.CreateMemStorage(0))
                {
                    Cv.Smooth(matBuf, matBuf, SmoothType.Gaussian, 9, 9, 4.0);
                    matBuf.Threshold(bin, 192.0, 255.0, ThresholdType.BinaryInv);
                    CvSeq<CvPoint> contour = null;
                    Cv.FindContours(bin, storage, out contour, OpenCvSharp.CvContour.SizeOf, ContourRetrieval.External, ContourChain.ApproxNone);
                    if (contour != null)
                    {
                        Cv.DrawContours(matBuf, contour, new CvScalar(100), new CvScalar(100), 10, -1);
                        contour.Dispose();
                    }
                    
                    for (int y = 0; y < cColorHeight; y++)
                    {
                        for (int x = 0; x < cColorWidth; x++)
                        {
                            int mirrorIndex = y * cColorWidth + (cColorWidth - 1 - x);
                            int colorIndex = y * cColorWidth + x;
                            pColorBuffer[mirrorIndex * 4 + 0] = pSilhouetteBuffer[colorIndex];
                            pColorBuffer[mirrorIndex * 4 + 1] = pSilhouetteBuffer[colorIndex];
                            pColorBuffer[mirrorIndex * 4 + 2] = pSilhouetteBuffer[colorIndex];
                            pColorBuffer[mirrorIndex * 4 + 3] = 255;
                        }
                    }

                    m_pSilhouetteX.LoadRawTextureData(pColorBuffer);
                    m_pSilhouetteX.Apply();
                    //Cv.ShowImage("Silhouette", matBuf);
                }
            }
        }

    }


    void Update()
    {
        // Get FPS
        elapsedCounter += Time.deltaTime;
        if (elapsedCounter > 1.0)
        {
            fps = frameCount / elapsedCounter;
            frameCount = 0;
            elapsedCounter = 0.0;
        }

        if (m_pMultiSourceFrameReader == null)
        {
            return;
        }

        if (!this.inGame)
        {
            return;
        }

        var pMultiSourceFrame = m_pMultiSourceFrameReader.AcquireLatestFrame();
        if (pMultiSourceFrame != null)
        {
            frameCount++;
            nullFrame = false;

            using (var pDepthFrame = pMultiSourceFrame.DepthFrameReference.AcquireFrame())
            {
                using (var pColorFrame = pMultiSourceFrame.ColorFrameReference.AcquireFrame())
                {
                    using (var pBodyIndexFrame = pMultiSourceFrame.BodyIndexFrameReference.AcquireFrame())
                    {
                        using (var pBodyFrame = pMultiSourceFrame.BodyFrameReference.AcquireFrame())
                        {

                            // Get Depth Frame Data.
                            if (pDepthFrame != null)
                            {
                                var pDepthData = GCHandle.Alloc(pDepthBuffer, GCHandleType.Pinned);
                                pDepthFrame.CopyFrameDataToIntPtr(pDepthData.AddrOfPinnedObject(), (uint)pDepthBuffer.Length * sizeof(ushort));
                                pDepthData.Free();
                            }

                            // Get BodyIndex Frame Data.
                            if (pBodyIndexFrame != null)
                            {
                                var pBodyIndexData = GCHandle.Alloc(pBodyIndexBuffer, GCHandleType.Pinned);
                                pBodyIndexFrame.CopyFrameDataToIntPtr(pBodyIndexData.AddrOfPinnedObject(), (uint)pBodyIndexBuffer.Length);
                                pBodyIndexData.Free();
                            }

                            //Ngwin Get Body Frame Data.
                            if (pBodyFrame != null)
                            {
                                if (this.bodies == null || this.bodies.Length != pBodyFrame.BodyCount)
                                {
                                    this.bodies = new Body[pBodyFrame.BodyCount];
                                }
                                pBodyFrame.GetAndRefreshBodyData(this.bodies);
                            }
                        }
                    }
                }
            }
            ProcessFrame();
        }
        else
        {
            nullFrame = true;
        }
    }

    void OnApplicationQuit()
    {
        pDepthBuffer = null;
        pColorBuffer = null;
        pBodyIndexBuffer = null;

        if (m_pDepthCoordinates != null)
        {
            m_pDepthCoordinates = null;
        }

        if (m_pMultiSourceFrameReader != null)
        {
            m_pMultiSourceFrameReader.Dispose();
            m_pMultiSourceFrameReader = null;
        }

        if (m_pKinectSensor != null)
        {
            m_pKinectSensor.Close();
            m_pKinectSensor = null;
        }
    }
}

