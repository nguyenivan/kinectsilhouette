﻿using UnityEngine;


public class SkeletonEnd
{
    public Vector3 ScreenPoint { get; set; }
    public Quaternion Orientation { get; set; }
    public SkeletonEnd()
    {
    }
    public SkeletonEnd(Vector3 point, Quaternion orientation)
    {
        this.ScreenPoint = point;
        this.Orientation = orientation;
    }
}

