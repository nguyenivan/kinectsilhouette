﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;
using OpenCvSharp;
using System.Runtime.InteropServices;
using System;

public class CoordinateMapperView : MonoBehaviour
{
    public GameObject CoordinateMapperManager;
    const int cColorWidth = 1920;
    const int cColorHeight = 1080;
    private CoordinateMapperManager _CoordinateMapperManager;
    private float width;
    private float height;
    private float screenToWorldX;
    private float screenToWorldY;

    void Start()
    {

        if (CoordinateMapperManager == null)
        {
            return;
        }

        _CoordinateMapperManager = CoordinateMapperManager.GetComponent<CoordinateMapperManager>();
        renderer.material.mainTexture = _CoordinateMapperManager.GetColorTexture();
        width = transform.localScale.x;
        height = transform.localScale.y;
        screenToWorldX = width / cColorWidth;
        screenToWorldY = height / cColorHeight;

    }

    void Update()
    {
        var head = _CoordinateMapperManager.GetSkeletonEnd(JointType.Head);
        var hand = _CoordinateMapperManager.GetSkeletonEnd(JointType.HandRight);

        var headSphere = GameObject.Find("Head");
        if (null != headSphere && null != head)
        {
            headSphere.transform.position = new Vector3(head.ScreenPoint.x * screenToWorldX - (width / 2), (height / 2) - head.ScreenPoint.y * screenToWorldY, 0);
        }

        var handSphere = GameObject.Find("RightHand");
        if (null != handSphere && null != hand)
        {
            handSphere.transform.position = new Vector3(hand.ScreenPoint.x * screenToWorldX - (width / 2), (height / 2) - hand.ScreenPoint.y * screenToWorldY, 0);
        }

        var pirateHat = GameObject.Find("PirateHat");
        if (null != pirateHat && null != head)
        {
            pirateHat.transform.position = new Vector3(head.ScreenPoint.x * screenToWorldX - (width / 2), (height / 2) - head.ScreenPoint.y * screenToWorldY, 5);
        }

        var pirateHook = GameObject.Find("PirateHook");
        if (null != pirateHook && null != hand)
        {
            pirateHook.transform.position = new Vector3(hand.ScreenPoint.x * screenToWorldX - (width / 2), (height / 2) - hand.ScreenPoint.y * screenToWorldY, 5);
            pirateHook.transform.rotation = hand.Orientation;
        }

    }

}
